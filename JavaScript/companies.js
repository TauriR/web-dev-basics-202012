async function getAllCompanies(){

    const result = await fetch('https://frogaccelerator.com:8110/companies');
    const companies = await result.json();
    console.log(companies);
    const companiesListElement = document.querySelector("#companiesList");
    let companiesHtml = "";
    companies.forEach(company => {
        companiesHtml = companiesHtml  + "<h1> "+ company.name + "</h1>" + '<button onclick="deleteCompany(' + company.id + ')"> Delete</button>' ;
    });
    companiesListElement.innerHTML = companiesHtml;
 }

 async function addCompany(){
     const name = document.querySelector('#name').value;
     const logo = document.querySelector('#logo').value;
     const employees = document.querySelector('#employees').value;
     const established = document.querySelector('#established').value;

     const company = {
         name: name,
         logo: logo,
         employees: employees,
         established: established
     };

     await fetch ('https://frogaccelerator.com:8110/companies', {
         method: 'POST',
         headers: {
             'Content-Type': 'application/json',
         },
         body: JSON.stringify(company)
     });

     getAllCompanies();
 }

 async function deleteCompany(id){
     await fetch('https://frogaccelerator.com:8110/companies/' + id, {
         method: 'DELETE'});
         getAllCompanies();
 }

 getAllCompanies();